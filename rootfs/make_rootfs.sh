#!/bin/bash
#
# Simple script to create a rootfs for aarch64 platforms including support
# for Kernel modules created by the rest of the scripting found in this
# module.
#
# Use this script to populate the second partition of disk images created with
# the simpleimage script of this project.
#

set -e

DEST="$1"
DISTRO="$2"
VARIANT="$3"
BUILD_ARCH="$4"
MODEL="$5"
Test="$6"
shift 6

export LC_ALL=C
if [ -z "$MODEL" ]; then
	echo "Usage: $0 <destination-folder> <distro: gentoo> <variant: i3> <arch: arm64> <model:rockpro64 rock64> <test/notest>"
	exit 1
fi

if [ "$(id -u)" -ne "0" ]; then
	echo "This script requires root."
	exit 1
fi

DEST=$(readlink -f "$DEST")
if [ -n "$LINUX" -a "$LINUX" != "-" ]; then
	LINUX=$(readlink -f "$LINUX")
fi

if [ ! -d "$DEST" ]; then
	echo "Destination $DEST not found or not a directory."
	exit 1
fi

if [ "$(ls -A -Ilost+found -Iboot $DEST)" ]; then
	echo "Destination $DEST is not empty. Aborting."
	exit 1
fi

if [ -z "$DISTRO" ]; then
	DISTRO="xenial"
fi

if [ -n "$BOOT" ]; then
	BOOT=$(readlink -f "$BOOT")
fi

TEMP=$(mktemp -d)
cleanup() {
	if [[ "$DEBUG" == "shell" ]]; then
		pushd "$DEST"
		bash
		popd
	fi
	umount "$DEST/proc/mdstat" || true
	umount "$DEST/proc" || true
	umount "$DEST/sys" || true
	umount "$DEST/tmp" || true
	#umount "$DEST/dev" || true
	rm -r "$TEMP"
}
trap cleanup EXIT

ROOTFS=""
TAR_OPTIONS=""
DISTRIB=""

case $DISTRO in
	gentoo)
		version="1"
		ROOTFS="http://distfiles.gentoo.org/experimental/arm64/stage3-arm64-20180907.tar.bz2"
		FALLBACK_ROOTFS="http://distfiles.gentoo.org/experimental/arm64/stage3-arm64-20180907.tar.bz2"
		TAR_OPTIONS=""
		DISTRIB="gentoo"
		EXTRA_ARCHS="arm64"
        ;;



    *)
		echo "Unknown distribution: $DISTRO"
		exit 1
		;;
esac


CACHE_ROOT="${CACHE_ROOT:-tmp}"
mkdir -p "$CACHE_ROOT"
TARBALL="${CACHE_ROOT}/$(basename $ROOTFS)"
if [ ! -e "$TARBALL" ]; then
	echo "Downloading $DISTRO rootfs tarball ..."
	pushd "$CACHE_ROOT"
	if ! flock "$(basename "$ROOTFS").lock" wget -c "$ROOTFS"; then
		TARBALL="${CACHE_ROOT}/$(basename "$FALLBACK_ROOTFS")"
		echo "Downloading fallback $DISTRO rootfs tarball ..."
		flock "$(basename "$FALLBACK_ROOTFS").lock" wget -c "$FALLBACK_ROOTFS"
	fi
	popd
fi
wget -P tmp/ -c "https://grmr.de/khaosgrille/boot.zip";
# Extract with BSD tar
echo -n "Extracting ... "
set -ex
tar -xf "$TARBALL" -C "$DEST" $TAR_OPTIONS
unzip "tmp/boot.zip" -d "$DEST"
echo "OK"

# Mount needed directories
mount -o bind /tmp "$DEST/tmp"

#my mounts
mount -t proc /proc "$DEST/proc"
mount -t sysfs /sys "$DEST/sys"
mount --bind /dev/null "$DEST/proc/mdstat"


##azufans mounts
#chroot "$DEST" mount -t proc proc /proc
#chroot "$DEST" mount -t sysfs sys /sys
#chroot "$DEST" mount --bind /dev/null /proc/mdstat
#mount --rbind /dev "$DEST/dev"
#mount --make-rslave "$DEST/dev"



# Mount var/apt/cache
# mkdir -p "$CACHE_ROOT/apt" "$DEST/var/cache/apt"
# mount -o bind "$CACHE_ROOT/apt" "$DEST/var/cache/apt"

# Add qemu emulation.
cp /usr/bin/qemu-aarch64-static "$DEST/usr/bin"
cp /usr/bin/qemu-arm-static "$DEST/usr/bin"

# Prevent services from starting
#cat > "$DEST/usr/sbin/policy-rc.d" <<EOF
#!/bin/sh
#exit 101
#EOF
#chmod a+x "$DEST/usr/sbin/policy-rc.d"

do_chroot() {
	chroot "$DEST" $CHROOT_PREFIX "$@"
}

do_install() {
	FILE=$(basename "$1")
	cp "$1" "$DEST/$FILE"
	yes | do_chroot apt install "/$FILE"
	rm -f "$DEST/$FILE"
}


# Configure system
cat > "$DEST/etc/hostname" <<EOF
$MODEL
EOF

cat > "$DEST/etc/fstab" <<EOF
LABEL=boot /boot/efi vfat defaults,sync 0 0
EOF

cat > "$DEST/etc/hosts" <<EOF
127.0.0.1 localhost
127.0.1.1 $MODEL

# The following lines are desirable for IPv6 capable hosts
::1     localhost ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
EOF

mkdir -p "$DEST/etc/flash-kernel"

cat > "$DEST/etc/flash-kernel/machine" <<EOF
en_US ISO-8859-1
en_US.UTF-8 UTF-8
de_DE ISO-8859-1
de_DE.UTF-8 UTF-8
EOF

# update everything
echo 'MAKEOPTS="-j8"' >> $DEST/etc/portage/make.conf

cp --dereference /etc/resolv.conf $DEST/etc/


if [ "$TEST" == "notest" ]; then
	rootfs/make_gentoo.sh   "$DEST" "$CHROOT_PREFIX" "$@"
fi


#if [ "$TEST" != "test" ]; then
 #   do_chroot emerge-webrsync
 #   do_chroot emerge --sync
 #   do_chroot eselect profile set default/linux/arm64/17.0
 #   do_chroot emerge --autounmask-continue sys-fs/libeatmydata
#    export CHROOT_PREFIX="eatmydata --"
#    do_chroot emerge --autounmask-continue --verbose --update --deep --newuse @world
#    do_chroot echo "Europe/Brussels" > /etc/timezone

#    cat > "$DEST/etc/flash-kernel/machine" <<EOF
#    $MODEL
#EOF
#    do_chroot eselect locale set en_US.UTF-8
#    do_chroot emerge --autounmask-continue   sys-apps/pciutils   sys-kernel/linux-firmware \
#    sys-fs/dosfstool curl app-arch/xz-utils net-wireless/iw  net-wireless/iw \
#    net-wireless/rfkill net-wireless/wpa_supplicant net-misc/openssh media-sound/alsa-utils \
#    dev-vcs/git app-editors/vim net-misc/wget app-misc/ca-certificates \
#    htop sys-apps/usbutils net-analyzer/fping net-misc/iperf:3 \
#    net-misc/ntp net-misc/networkmanager sys-process/psmisc dev-embedded/u-boot-tools \
#    sys-apps/net-tools  sys-fs/mtd-utils net-misc/rsync sys-apps/dtc \
 #   sys-kernel/linux-firmware

#fi

###do_chroot fake-hwclock save ### maybe use something like swclock?



# Bring back folders
mkdir -p "$DEST/lib"
mkdir -p "$DEST/usr"




 # Clean up
rm -f "$DEST/usr/bin/qemu-arm-static"
rm -f "$DEST/usr/bin/qemu-aarch64-static"
rm -f "$DEST/usr/sbin/policy-rc.d"
rm -f "$DEST/usr/local/bin/mdadm"
rm -f "$DEST/var/lib/dbus/machine-id"
rm -f "$DEST/etc/flash-kernel/machine"
rm -f "$DEST/SHA256SUMS"

echo "Done - installed rootfs to $DEST"
