#!/bin/bash
set -e

DEST="$1"
CHROOT_PREFIX="$2"
shift 2








do_chroot() {
    chroot "$DEST" $CHROOT_PREFIX "$@"
}



do_chroot emerge-webrsync
do_chroot emerge --sync
do_chroot eselect profile set default/linux/arm64/17.0
do_chroot emerge --autounmask-continue sys-fs/libeatmydata
export CHROOT_PREFIX="eatmydata --"
#do_chroot emerge --autounmask-continue --verbose --update --deep --newuse @world
do_chroot echo "Europe/Brussels" > /etc/timezone

cat > "$DEST/etc/flash-kernel/machine" <<EOF
$MODEL
EOF
do_chroot eselect locale set en_US.UTF-8
#do_chroot emerge --autounmask-continue   sys-apps/pciutils   sys-kernel/linux-firmware \
#sys-fs/dosfstool curl app-arch/xz-utils net-wireless/iw  net-wireless/iw \
#net-wireless/rfkill net-wireless/wpa_supplicant net-misc/openssh media-sound/alsa-utils \
#dev-vcs/git app-editors/vim net-misc/wget app-misc/ca-certificates \
#htop sys-apps/usbutils net-analyzer/fping net-misc/iperf:3 \
#net-misc/ntp net-misc/networkmanager sys-process/psmisc dev-embedded/u-boot-tools \
#sys-apps/net-tools  sys-fs/mtd-utils net-misc/rsync sys-apps/dtc \
#sys-kernel/linux-firmware


echo "rock64" | passwd --stdin
