#!/bin/bash
#
# This scripts takes a simpleimage and a kernel tarball, resizes the
# secondary partition and creates a rootfs inside it. Then extracts the
# Kernel tarball on top of it, resulting in a full Pine64 disk image.

OUT_IMAGE="$1"
DISTRO="$2"
VARIANT="$3"
BUILD_ARCH="$4"
MODEL="$5"
TEST="$6"

shift 6

if [[ -z "$DISTRO" ]] || [[ -z "$VARIANT" ]] || [[ -z "$BUILD_ARCH" ]] || [[ -z "$MODEL" ]]; then
	echo "Usage: $0 <disk.img> <distro> <variant: mate, i3 or minimal> <arch> <model> <test/notest> <packages...>"
    echo "Empty DISTRO, VARIANT, BUILD_ARCH or MODEL."
	exit 1
fi

if [[ "$(id -u)" -ne "0" ]]; then
	echo "This script requires root."
	exit 1
fi

case "$VARIANT" in
    minimal)
        SIZE=15046
        ;;
    *)
        echo "Unknown VARIANT: $VARIANT"
        exit 1
        ;;
esac

PWD=$(readlink -f .)
TEMP=$(mktemp -p $PWD -d -t "$MODEL-build-XXXXXXXXXX")
echo "> Building in $TEMP ..."

cleanup() {
    echo "> Cleaning up ..."
    umount "$TEMP/rootfs/boot/efi" || true
    umount "$TEMP/rootfs/"* || true
    umount "$TEMP/rootfs" || true
    kpartx -d "${LODEV}" || true
    losetup -d "${LODEV}" || true
    rm -rf "$TEMP"
}
trap cleanup EXIT

TEMP_IMAGE="${OUT_IMAGE}.tmp"

set -ex

# Create
rm -f "$TEMP_IMAGE"
dd if=/dev/zero of="$TEMP_IMAGE" bs=1M seek=$((SIZE-1)) count=0

# Create partitions
echo Updating GPT...
parted -s "${TEMP_IMAGE}" mklabel gpt
parted -s "${TEMP_IMAGE}" unit s mkpart loader1      64 8063      # 4MB
parted -s "${TEMP_IMAGE}" unit s mkpart reserved1    8064 8191    # 4MB
parted -s "${TEMP_IMAGE}" unit s mkpart reserved2    8192 16383   # 4MB
parted -s "${TEMP_IMAGE}" unit s mkpart loader2      16384 24575  # 4MB
parted -s "${TEMP_IMAGE}" unit s mkpart atf          24576 32767  # 4MB
parted -s "${TEMP_IMAGE}" unit s mkpart boot fat16   32768 262143 # 128MB
parted -s "${TEMP_IMAGE}" unit s mkpart root ext4    262144 100%  # rest
parted -s "${TEMP_IMAGE}" set 7 legacy_boot on

# Assign lodevice
LODEV=$(losetup -f --show "${TEMP_IMAGE}")

# Map path from /dev/loop to /dev/mapper/loop
LODEVMAPPER="${LODEV/\/dev\/loop/\/dev\/mapper\/loop}"

# Assign partitions
kpartx -a "$LODEV"

# Make filesystem
mkfs.vfat -n "boot" -S 512 "${LODEVMAPPER}p6"
mkfs.ext4 -i 50000 -L "linux-root" "${LODEVMAPPER}p7"
tune2fs -o journal_data_writeback "${LODEVMAPPER}p7"

# Mount filesystem
mkdir -p "$TEMP/rootfs"
mount -o data=writeback,commit=3600 "${LODEVMAPPER}p7" "$TEMP/rootfs"
mkdir -p "$TEMP/rootfs/boot/efi"
mount "${LODEVMAPPER}p6" "$TEMP/rootfs/boot/efi"

#Tell me if test

if [ "$TEST" == "test" ]; then
	echo "This script is in test modus and will not emerge anything!"
fi





# Create image
rootfs/make_rootfs.sh "$TEMP/rootfs" "$DISTRO" "$VARIANT" "$BUILD_ARCH" "$MODEL" "$TEST" "$@"

## Write bootloader  #need to add this and add this tp the todo. currentloy i expect an spl flash
#dd if="$TEMP/rootfs/usr/lib/u-boot-${MODEL}/rksd_loader.img" of="${LODEVMAPPER}p1"

cat > "$TEMP/rootfs/boot/efi/extlinux/extlinux.conf" <<EOF
timeout 10
default kernel-latest
menu title select kernel

label kernel-latest
    kernel /Image
    initrd /initrd.img
    fdt /dtb
    append rw root=LABEL=linux-root rootwait rootfstype=ext4 panic=10 init=/sbin/init coherent_pool=1M ethaddr=${ethaddr} eth1addr=${eth1addr} serial=${serial#} cgroup_enable=cpuset cgroup_memory=1 cgroup_enable=memory swapaccount=1

label kernel-previous
    kernel /Image.bak
    initrd /initrd.img.bak
    fdt /dtb.bak
    append rw root=LABEL=linux-root rootwait rootfstype=ext4 panic=10 init=/sbin/init coherent_pool=1M ethaddr=${ethaddr} eth1addr=${eth1addr} serial=${serial#} cgroup_enable=cpuset cgroup_memory=1 cgroup_enable=memory swapaccount=1
EOF

# Umount filesystem
sync -f "$TEMP/rootfs" "$TEMP/rootfs/boot/efi"
fstrim "$TEMP/rootfs"
df -h "$TEMP/rootfs" "$TEMP/rootfs/boot/efi"

# Cleanup build
cleanup
trap - EXIT

# Verify partitions
parted -s "${TEMP_IMAGE}" print

# Move image into final location
mv "$TEMP_IMAGE" "$OUT_IMAGE"

echo "end of build-system-image.sh"
